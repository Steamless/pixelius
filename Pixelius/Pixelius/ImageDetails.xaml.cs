﻿using Acr.UserDialogs;
using Pixelius.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pixelius
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImageDetails : ContentPage
            
	{

        public ObservableCollection<PhotoTable> listViewData { get; set; }
        bool settingUp;

        private PhotoTable photo;

        public ImageDetails (PhotoTable p)
		{
			InitializeComponent ();

            photo = p;
            DetailPhotoImage.Source = ImageSource.FromFile(photo.Filename);

            settingUp = true;
            settingUp = false;

        }

        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            await App.Current.MainPage.Navigation.PushAsync(new ImageDetails((PhotoTable)e.SelectedItem));

            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            //Wenn man den Löschen Button drückt
            var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
            {
                Message = "Wanna really delete it? >< ",
                OkText = "Yup",
                CancelText = "Wait..."
            });

            if (result)
            {
                await HelperDB.Instance.DeletePhoto(photo.Photo_id);

                //To go back to main menu after deleting the picture
                await Navigation.PopAsync();

            }



        }

        public class ViewModelBase : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }
    }

}