﻿using Pixelius.Utils;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Pixelius
{
	public partial class MainPage : ContentPage
	{

        public ObservableCollection<PhotoTable> listViewData { get; set; }

        public MainPage()
		{
			InitializeComponent();

            listViewPhoto.ItemSelected += OnSelection;

            UpdateChildrenLayout();
        }



        protected override void OnAppearing()
        {
            UpdateChildrenLayout();

            base.OnAppearing();

            loadImages();
        }

        //Gallery Logic

        //Ordnerwahl

        async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            await App.Current.MainPage.Navigation.PushAsync(new ImageDetails((PhotoTable)e.SelectedItem));

            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }

        async private void loadImages()
        {
            listViewData = new ObservableCollection<PhotoTable>();
            var f = await HelperDB.Instance.GetPhotos();
            foreach (var item in f)
            {
                listViewData.Add(item);
            }

            listViewPhoto.ItemsSource = listViewData;

        }

        //Camera Logic

        public async Task<bool> CheckPhotoPermissionsAsync()
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

            if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                cameraStatus = results[Permission.Camera];
                storageStatus = results[Permission.Storage];
            }

            return cameraStatus == PermissionStatus.Granted || storageStatus == PermissionStatus.Granted;
        }

        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            //Toma la foto y la muestra directamente en la pagina
            //Macht ein Foto und zeigt es auf diese Seite

            bool photoPermissions = await CheckPhotoPermissionsAsync();

            if (!photoPermissions)
            {
                await DisplayAlert("Achtung!", "Damit die App einwandfrei läuft, müssen sie den Berechtigungen zustimmen", "ok");
                return;
            }
            else
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    //If camera option not available
                    //Falls das Handy keine Kamera hat
                    await DisplayAlert("Hmmm...!", "Das Gerät hat kein Zugriff auf die Kamera...", "ok");
                    return;
                }

#if __IOS__
                var mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    SaveToAlbum = true,
                    //Directory = "Pixelius",
                    Name = fileName(),

                    //MaxWidthHeight = 600,
                    //PhotoSize = PhotoSize.MaxWidthHeight,

                    PhotoSize = PhotoSize.Custom,

                    //SaveMetaData = false,

                    //Nurr bei PhotoSize.Custom
                    CustomPhotoSize = 17,
                    CompressionQuality = 100, //eventuelle Bildkompression

                    //Only for iOS
                    AllowCropping = false

                });
#endif

#if __ANDROID__
                var mediaFile = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    SaveToAlbum = true,

                    Directory = "Pixelius",
                    Name = fileName(),

                 //Bei feste Größe
                    //MaxWidthHeight = 600,
                    //PhotoSize = PhotoSize.MaxWidthHeight,

                    CompressionQuality = 1,

                    //RotateImage = true,

                    //SaveMetaData = false,

                    //Nurr bei PhotoSize.Custom
                    PhotoSize = PhotoSize.Custom,
                    CustomPhotoSize = 100,


                    //Only for iOS
                    AllowCropping = false

                });
#endif

                await saveMediaFile(mediaFile);

#if __ANDROID__
                var x = ImageSource.FromStream(() =>
                {

                    //var stream = mediaFile.GetStreamWithImageRotatedForExternalStorage();
                    var stream = mediaFile.GetStream();
                    //mediaFile.Dispose();
                    return stream;

                });
#endif

#if __IOS__
                var x = ImageSource.FromStream(() =>
                {

                    var stream = mediaFile.GetStreamWithImageRotatedForExternalStorage();
                    //var stream = mediaFile.GetStream();
                    //mediaFile.Dispose();
                    return stream;

                });
#endif

                Debug.WriteLine("It works baby! The mediaFile is: " + mediaFile);

            }


        }

        private async Task saveMediaFile(MediaFile mediaFile)
        {
            if (mediaFile == null)
            {
                //Falls kein Bild vorhanden ist - wird nicht wirklich gebraucht
                //await DisplayAlert("Ups!", "Es ist kein Bild vorhanden", "ok");
                return;
            }
            try
            {
                var xxx = mediaFile.Path;

                //PhotoTable ist eine Klasse in diesem Fall , die die Attribute vom DB hat.

                var s = new PhotoTable
                {
                    Filename = mediaFile.Path,
                    Created_At = DateTime.Now,
                    Sync_At = null,
                };

                await HelperDB.Instance.conn.InsertAsync(s);
                //PhotoImage.Source = ImageSource.FromStream(() => mediaFile.GetStream());

            }
            catch (Exception ee)
            {
                //Falls was nicht mit dem DB klappt....
                await DisplayAlert("Ups!", "Bitte versuchen Sie das nochmal. Es gab ein problem mit dem DatenBank" + ee.ToString(), "ok");

            }

            loadImages();

        }

        public string fileName()
        {
            DateTime d = DateTime.Now;

            string fileName = $"Pixelius{d.ToString("yyyyMMdd_HHmmss")}_.jpg";

            return fileName;
        }

        private async void SelectImageButton_Clicked(object sender, EventArgs e)
        {
            //selecciona imagenes de la galeria y las muestra en la pagina
            //Wählt ein Bild vom Gallery und zeigt sie auf diese Seite

            bool photoPermissions = await CheckPhotoPermissionsAsync();

            if (!photoPermissions)
            {
                await DisplayAlert("Ayayayay!", "Damit die App einwandfrei läuft, müssen sie den Berechtigungen zustimmen", "ok");
                return;
            }
            else
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    //if picking option not available
                    //Falls das Handy keine Objekte bzw. Bilder wählen kann (Die Option dafür)
                    await DisplayAlert("Ayayayay!", "Das Gerät hat kein 'auswählen' Option...", "ok");
                    return;
                }
                else
                {
                    //Method to enables picking images from gallery
                    var mediafile = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        PhotoSize = PhotoSize.Custom,

                        //SaveMetaData = false,

                        //Nurr bei PhotoSize.Custom
                        CustomPhotoSize = 100,
                        CompressionQuality = 1,

                    });

                    await saveMediaFile(mediafile);

                    //await saveScaleAsync(mediaFile);

                }
            }
        }
    }
}
		