﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixelius.Utils
{
    public class PhotoTable
    {
        //Die Attribute sozusagen von der Phototabelle des DBs
        [PrimaryKey, AutoIncrement]

        public int Photo_id { get; set; }
        public string Filename { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime? Sync_At { get; set; }

    }
}
