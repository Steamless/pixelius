﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Pixelius.Utils
{
    public class HelperDB
    {
        public SQLiteAsyncConnection conn;
        static readonly HelperDB _instance = new HelperDB();
        public static HelperDB Instance
        {
            get
            {
                return _instance;
            }
        }

        //DB for the pictures. 

        public HelperDB()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var file = System.IO.Path.Combine(folder, "ewes.sqlite");
            bool createTables = true;
            if (File.Exists(file))
            {
                createTables = false;
            }
            conn = new SQLiteAsyncConnection(file);

            if (createTables)
            {
                newDB();
            }

        }

        private async void newDB()
        {
            await conn.CreateTableAsync<PhotoTable>();
        }

        public async Task<List<PhotoTable>> GetPhotos()
        {
            return await conn.QueryAsync<PhotoTable>("SELECT * FROM PhotoTable ORDER BY created_at DESC;");
        }

        public async Task<int> UpdatePhotoMediadir(int id, int mediadirID)
        {
            return await conn.ExecuteAsync($"UPDATE PhotoTable set MediaDir_id = {mediadirID} where Photo_id ={id};");
        }


        public async Task<int> UpdatePhoto(int id, string u1, string u2)
        {
            return await conn.ExecuteAsync($"UPDATE PhotoTable set Sync_At = DATETIME('now','localtime')  , UUID1 =  '{u1}' , UUID2 =  '{u2}' where Photo_id = {id};");
        }

        public async Task<int> UpdatePhoto(PhotoTable photo)
        {
            return await conn.UpdateAsync(photo);

        }

        public async Task<int> DeletePhoto(int photo_id)
        {
            return await conn.ExecuteAsync("DELETE from PhotoTable where Photo_id = ?;", photo_id);
        }



    }
}
