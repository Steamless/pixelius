﻿using Pixelius.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Pixelius
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            MainPage = new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex("#002f38"), BarTextColor = Color.White };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            var db = HelperDB.Instance;

            MainPage = new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex("#002f38"), BarTextColor = Color.White };

        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
